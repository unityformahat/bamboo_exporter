import unittest
from cicd_exporter.bamboo_exporter import Bamboo
from atlassian import Bamboo as BambooSession  # atlassian api tool for python
from cicd_exporter.cicd import *


class TestBamboo(unittest.TestCase):
    def test_bamboo_private_ctor(self):
        bamboo = Bamboo.get_instance()
        self.assertTrue(bamboo.get_projects() == [] and bamboo.get_plans() == [] and
                        bamboo.get_builds() == [] and isinstance(bamboo.get_session(), BambooSession))

    def test_parse_projects(self):
        projects_sample_data_1 = []
        projects_sample_data_2 = [{'key': 'FP', 'name': 'first project', 'description': 'very nice prect',
                                   'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/project/FP',
                                            'rel': 'self'}},
                                  {'key': 'UN', 'name': 'unity-test', 'description': 'testing api',
                                   'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/project/UN',
                                            'rel': 'self'}}]
        bamboo = Bamboo.get_instance()
        bamboo._Bamboo__parse_projects_data(projects_sample_data_1)
        self.assertEqual(len(bamboo.get_projects()), len(projects_sample_data_1))
        bamboo._Bamboo__parse_projects_data(projects_sample_data_2)
        self.assertEqual(len(bamboo.get_projects()), len(projects_sample_data_2))

    def test_insert_project(self):
        bamboo = Bamboo.get_instance()
        bamboo.insert_project(CICDProject('abc', 'abc'))
        self.assertEqual(len(bamboo.get_projects()), 1)
        self.assertTrue(isinstance(bamboo.get_projects()[0], tuple))
        self.assertFalse(isinstance(bamboo.get_projects()[0], CICDProject))
        with self.assertRaises(TypeError):
            bamboo.insert_project(1)
        with self.assertRaises(TypeError):
            bamboo.insert_project([1])
        with self.assertRaises(TypeError):
            bamboo.insert_project("abc")
        bamboo._CICDTool__projects = []

    def test_parse_plans(self):
        plans_sample_data_1 = []
        plans_sample_data_2 = [{'shortName': 'my plan', 'shortKey': 'MP', 'type': 'chain', 'enabled': True,
                                'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/plan/FP-MP',
                                         'rel': 'self'}, 'key': 'FP-MP', 'name': 'first project - my plan',
                                'planKey': {'key': 'FP-MP'}},
                               {'shortName': 'test', 'shortKey': 'TEST', 'type': 'chain', 'enabled': False,
                                'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/plan/UN-TEST',
                                         'rel': 'self'}, 'key': 'UN-TEST', 'name': 'unity-test - test',
                                'planKey': {'key': 'UN-TEST'}}]
        bamboo = Bamboo.get_instance()
        bamboo._Bamboo__parse_plans_data(plans_sample_data_1)
        self.assertEqual(len(bamboo.get_plans()), len(plans_sample_data_1))
        bamboo._Bamboo__parse_plans_data(plans_sample_data_2)
        self.assertEqual(len(bamboo.get_plans()), len(plans_sample_data_2) - 1)

    def test_insert_plan(self):
        bamboo = Bamboo.get_instance()
        bamboo.insert_plan(CICDPlan('abc', 'abc', 'abc'))
        self.assertEqual(len(bamboo.get_plans()), 1)
        self.assertTrue(isinstance(bamboo.get_plans()[0], tuple))
        self.assertFalse(isinstance(bamboo.get_plans()[0], CICDPlan))
        with self.assertRaises(TypeError):
            bamboo.insert_plan(1)
        with self.assertRaises(TypeError):
            bamboo.insert_plan([1])
        with self.assertRaises(TypeError):
            bamboo.insert_plan("abc")
        bamboo._CICDTool__plans = []

    def test_parse_builds(self):
        builds_sample_data_1 = []
        builds_sample_data_2 = [
            {'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/result/FP-MP-20', 'rel': 'self'},
             'plan': {'shortName': 'my plan', 'shortKey': 'MP', 'type': 'chain', 'enabled': True,
                      'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/plan/FP-MP', 'rel': 'self'},
                      'key': 'FP-MP', 'name': 'first project - my plan', 'planKey': {'key': 'FP-MP'}},
             'buildResultKey': 'FP-MP-20', 'lifeCycleState': 'Finished', 'id': 884855, 'specsResult': False,
             'key': 'FP-MP-20', 'planResultKey': {'key': 'FP-MP-20', 'entityKey': {'key': 'FP-MP'}, 'resultNumber': 20},
             'state': 'Successful', 'buildState': 'Successful', 'number': 20, 'buildNumber': 20},
            {'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/result/FP-MP-19', 'rel': 'self'},
             'plan': {'shortName': 'my plan', 'shortKey': 'MP', 'type': 'chain', 'enabled': True,
                      'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/plan/FP-MP', 'rel': 'self'},
                      'key': 'FP-MP', 'name': 'first project - my plan', 'planKey': {'key': 'FP-MP'}},
             'buildResultKey': 'FP-MP-19', 'lifeCycleState': 'Finished', 'id': 884853, 'specsResult': False,
             'key': 'FP-MP-19', 'planResultKey': {'key': 'FP-MP-19', 'entityKey': {'key': 'FP-MP'}, 'resultNumber': 19},
             'state': 'Successful', 'buildState': 'Successful', 'number': 19, 'buildNumber': 19},
            {'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/result/FP-MP-18', 'rel': 'self'},
             'plan': {'shortName': 'my plan', 'shortKey': 'MP', 'type': 'chain', 'enabled': True,
                      'link': {'href': 'http://52.169.187.138:8085/rest/api/latest/plan/FP-MP', 'rel': 'self'},
                      'key': 'FP-MP', 'name': 'first project - my plan', 'planKey': {'key': 'FP-MP'}},
             'buildResultKey': 'FP-MP-18', 'lifeCycleState': 'Finished', 'id': 884851, 'specsResult': False,
             'key': 'FP-MP-18', 'planResultKey': {'key': 'FP-MP-18', 'entityKey': {'key': 'FP-MP'}, 'resultNumber': 18},
             'state': 'Failed', 'buildState': 'Failed', 'number': 18, 'buildNumber': 18}
        ]

        bamboo = Bamboo.get_instance()
        bamboo._Bamboo__parse_builds_data(builds_sample_data_1, 'MP', 'FP')
        self.assertEqual(len(bamboo.get_builds()), 0)
        bamboo._Bamboo__parse_builds_data(builds_sample_data_2, 'MP', 'FP')
        self.assertEqual(len(bamboo.get_builds()), 3)

    def test_parse_build_reason(self):
        build_reason_sample_1 = 'Changes by' \
                                ' <a href="http://52.169.187.138:8085/authors/viewAuthor.action?authorName=' \
                                'devops%20%3Cdevops%40devops.com%3E">devops &lt;devops@devops.com&gt;</a>'
        build_reason_sample_2 = 'Manual run by <a href="http://52.169.187.138:8085/browse/user/admin">admin</a>'

        bamboo = Bamboo.get_instance()
        self.assertEqual(bamboo._Bamboo__parse_build_reason(build_reason_sample_1),
                         'devops%20%3Cdevops%40devops.com%3E"')
        self.assertEqual(bamboo._Bamboo__parse_build_reason(build_reason_sample_2), "admin")

    def test_insert_build(self):
        bamboo = Bamboo.get_instance()
        bamboo.insert_build(CICDBuild(884855, 20, '2020-06-01T10:17:38.773Z', 'True', 'admin', 0, 0, 'MP'))
        self.assertEqual(len(bamboo.get_builds()), 1)
        self.assertTrue(isinstance(bamboo.get_builds()[0], tuple))
        self.assertFalse(isinstance(bamboo.get_builds()[0], CICDPlan))
        with self.assertRaises(TypeError):
            bamboo.insert_build(1)
        with self.assertRaises(TypeError):
            bamboo.insert_build([1])
        with self.assertRaises(TypeError):
            bamboo.insert_build("abc")
        bamboo._CICDTool__builds = []
