import unittest
from cicd_exporter.cicd import *


class TestCICDProject(unittest.TestCase):
    def test_ci_cd_project_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            CICDProject()
        with self.assertRaises(TypeError):
            CICDProject("1")


class TestCICDPlan(unittest.TestCase):
    def test_ci_cd_repository_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            CICDPlan("abc")
        with self.assertRaises(TypeError):
            CICDPlan("abc", "abc")


class TestCICDBuild(unittest.TestCase):
    def test_ci_cd_build_ctor_missing_input_arguments(self):
        with self.assertRaises(TypeError):
            CICDBuild()
        with self.assertRaises(TypeError):
            CICDBuild(1)
        with self.assertRaises(TypeError):
            CICDBuild(4, 2, '2020-05-06T09:43:38.958Z', 1)

    def test_ci_cd_build_ctor_false_build_id_input(self):
        with self.assertRaises(ValueError):
            CICDBuild("bla", 6, '2020-05-06T09:43:38.958Z', True, 'admin', 0, 0, 'REAC')
        with self.assertRaises(TypeError):
            CICDBuild(None, 6, '2020-05-06T09:43:38.958Z', True, 'admin', 0, 0, 'REAC')

    def test_ci_cd_build_ctor_false_build_number_input(self):
        with self.assertRaises(ValueError):
            CICDBuild(884769, "bla", '2020-05-06T09:43:38.958Z', True, 'admin', 0, 0, 'REAC')
        with self.assertRaises(TypeError):
            CICDBuild(884769, None, '2020-05-06T09:43:38.958Z', True, 'admin', 0, 0, 'REAC')

    def test_ci_cd_build_ctor_false_build_successful_tests_count_input(self):
        with self.assertRaises(ValueError):
            CICDBuild(884769, 6, '2020-05-06T09:43:38.958Z', True, 'admin', "bla", 0, 'REAC')
        with self.assertRaises(TypeError):
            CICDBuild(884769, 6, '2020-05-06T09:43:38.958Z', True, 'admin', None, 0, 'REAC')

    def test_ci_cd_build_ctor_false___build_failed_tests_count_input(self):
        with self.assertRaises(ValueError):
            CICDBuild(884769, 6, '2020-05-06T09:43:38.958Z', True, 'admin', 0, "bla", 'REAC')
        with self.assertRaises(TypeError):
            CICDBuild(884769, 6, '2020-05-06T09:43:38.958Z', True, 'admin', 0, None, 'REAC')
