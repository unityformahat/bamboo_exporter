#! /usr/bin/env python

from atlassian import Bamboo as BambooSession  # atlassian api tool for python
import requests
from exporterutilities.DbMannager import DbMannager
from cicd_exporter.cicd import *

BAMBOO_URL = 'http://52.169.187.138:8085/'
BAMBOO_USER = 'admin'
BAMBOO_PASSWORD = 'admin'


class Bamboo(CICDTool):
    __instance = None

    def __init__(self):
        if Bamboo.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            super(Bamboo, self).__init__()
            self.set_session(BambooSession(url=BAMBOO_URL, username=BAMBOO_USER, password=BAMBOO_PASSWORD))
            Bamboo.__instance = self

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Bamboo.__instance is None:
            Bamboo()
        return Bamboo.__instance

    def fetch_projects(self):
        try:
            projects_data = list(self.get_session().projects())
            self.__parse_projects_data(projects_data)
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_projects_data(self, projects_data):
        for project in projects_data:
            self.insert_project(CICDProject(project["key"], project["name"]))

    def fetch_plans(self):
        """"Returns all bamboo`s repos via list projects."""
        try:
            plans_data = list(self.get_session().plans())
            self.__parse_plans_data(plans_data)
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_plans_data(self, plans_data):
        for plan in plans_data:
            if 'enabled' in plan and plan["enabled"]:
                project_key = plan["key"].split("-")[0]
                self.insert_plan(CICDPlan(plan['shortKey'], plan['shortName'], project_key))

    def fetch_builds(self):
        try:
            plans = self.get_plans()
            if len(plans) == 0:
                self.fetch_plans()
            for plan in plans:
                plan_key = plan[0]
                project_key = plan[2]
                builds_data = list(self.get_session().plan_results(plan_key=plan_key, project_key=project_key))
                self.__parse_builds_data(builds_data, plan_key, project_key)
        except ValueError as projectError:
            print("Failed to initialize a project, Error: %", projectError)

    def __parse_builds_data(self, builds_data, plan_key, project_key):
        for build in builds_data:
            build_number = build["buildNumber"]
            self.__fetch_build_data(project_key, plan_key, build_number)

    def __fetch_build_data(self, project_key, plan_key, build_number):
        try:
            request = requests.get(
                self.get_session().url + "rest/api/latest/result/" + project_key + "-" + plan_key + "-" + str(
                    build_number) + ".json",
                auth=(BAMBOO_USER, BAMBOO_PASSWORD)
            )
            build_data = request.json()
            self.__parse_build_data(build_data, build_number, plan_key)

        except ValueError as error:
            print("Failed to initialize build, Error: % ", error)

    def __parse_build_data(self, build_data, build_number, plan_key):
        build_reason = build_data["buildReason"]
        author_name = self.__parse_build_reason(build_reason)
        if build_data["buildState"] == "Successful":
            build_state = True
        else:
            build_state = False
        self.insert_build(CICDBuild(build_data["id"], build_number, build_data["buildStartedTime"],
                                    build_state, author_name, build_data["successfulTestCount"],
                                    build_data["failedTestCount"], plan_key))

    @staticmethod
    def __parse_build_reason(build_reason):
        if build_reason.find('authorName') >= 0:
            author_name_substring_start = build_reason.find('authorName') + 11
            author_name_substring = build_reason[author_name_substring_start:len(build_reason)]
            author_name = author_name_substring.split(">")[0]
        else:
            substring_start = build_reason.find(">") + 1
            author_substring = build_reason[substring_start:len(build_reason)]
            author_name = author_substring.split("<")[0]
        return author_name


def main():
    bamboo = Bamboo.get_instance()
    print("**Fetching Projects**")
    bamboo.fetch_projects()
    print(bamboo.get_projects())
    print("**Fetching Plans**")
    bamboo.fetch_plans()
    print(bamboo.get_plans())
    print("**Fetching Builds**")
    bamboo.fetch_builds()
    print(bamboo.get_builds())
    # sending all info to postgresql
    dbm = DbMannager()
    dbm.postgressql_execute("bamboo_projects", bamboo.get_projects())
    dbm.postgressql_execute("bamboo_plans", bamboo.get_plans())
    dbm.postgressql_execute("bamboo_builds", bamboo.get_builds())

    dbm.close_connection()


if __name__ == '__main__':
    main()
