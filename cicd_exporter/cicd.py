from abc import ABC, abstractmethod
import datetime


class CICDProject:
    def __init__(self, project_key, project_name):
        self.__project_key = str(project_key)
        self.__project_name = str(project_name)

    def get_project_key(self):
        return self.__project_key

    def get_project_name(self):
        return self.__project_name


class CICDPlan:
    def __init__(self, plan_key, plan_name, plan_project_key):
        self.__plan_key = str(plan_key)
        self.__plan_name = str(plan_name)
        self.__plan_project_key = str(plan_project_key)

    def get_plan_key(self):
        return self.__plan_key

    def get_plan_name(self):
        return self.__plan_name

    def get_plan_project_key(self):
        return self.__plan_project_key


class CICDBuild:
    def __init__(self, build_id, build_number, build_start_time, build_state, builder_name,
                 build_successful_tests_count, build_failed_tests_count, build_plan_key):
        self.__build_id = int(build_id)
        self.__build_number = int(build_number)
        self.__build_start_time = str(build_start_time)
        self.__build_state = str(build_state)
        self.__builder_name = str(builder_name)
        self.__build_successful_tests_count = int(build_successful_tests_count)
        self.__build_failed_tests_count = int(build_failed_tests_count)
        self.__build_plan_key = str(build_plan_key)

    def get_build_id(self):
        return self.__build_id

    def get_build_number(self):
        return self.__build_number

    def get_build_start_time(self):
        return self.__build_start_time

    def get_build_state(self):
        return self.__build_state

    def get_builder_name(self):
        return self.__builder_name

    def get_build_successful_tests_count(self):
        return self.__build_successful_tests_count

    def get_build_failed_tests_count(self):
        return self.__build_failed_tests_count

    def get_build_plan_key(self):
        return self.__build_plan_key


class CICDTool(ABC):
    def __init__(self, projects=[], plans=[], builds=[], session=None):
        self.__projects = list(projects)
        self.__plans = list(plans)
        self.__builds = list(builds)
        self.__session = session

    def get_projects(self):
        """Returns all projects via list object."""
        return self.__projects

    def get_plans(self):
        """"Returns all plans via list object."""
        return self.__plans

    def get_builds(self):
        """"Returns all builds via list projects."""
        return self.__builds

    def get_session(self):
        """"Returns connection session"""
        return self.__session

    def set_session(self, session):
        self.__session = session

    def insert_project(self, project=None):
        """Inserts a project to the project list"""
        if isinstance(project, CICDProject):
            self.__projects.append((project.get_project_key(), project.get_project_name()))
        else:
            raise TypeError("project should be a CICDProject")

    def insert_plan(self, plan=None):
        """Inserts a plan to the plan list"""
        if isinstance(plan, CICDPlan):
            self.__plans.append((plan.get_plan_key(), plan.get_plan_name(), plan.get_plan_project_key()))
        else:
            raise TypeError("plan should be a CICDPlan")

    def insert_build(self, build=None):
        """Inserts a build to the build list"""
        if isinstance(build, CICDBuild):
            self.__builds.append((build.get_build_id(), build.get_build_number(), build.get_build_start_time(),
                                  build.get_build_state(), build.get_builder_name(),
                                  build.get_build_successful_tests_count(), build.get_build_failed_tests_count(),
                                  build.get_build_plan_key()))
        else:
            raise TypeError("build should be a CICDBuild")

    @abstractmethod
    def fetch_projects(self):
        pass

    @abstractmethod
    def fetch_plans(self):
        pass

    @abstractmethod
    def fetch_builds(self):
        pass
