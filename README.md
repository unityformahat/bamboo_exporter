The CI-CD exporter is a one of numerous data exporters that are meant to fetch development process related data. 
This exporter fetches the following data from a ci-cd tool:

* Projects
* Plans
* Builds

Currently the exporter is fetching from the following CI-CD tools:
* Bamboo

The data is inserted into a PostgreSQL DB, and used later by the client service in Unity.